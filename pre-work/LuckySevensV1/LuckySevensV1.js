function hideResults(){
    document.getElementById("results").style.display = "none";
}

function play() {
    
    var startingBet = document.getElementById("betInput").value;
    var bet = parseInt(startingBet);
    var initialBet = bet;
    var currentStep = 0;
    var maxBet = bet;
    var maxBetStep = 0;


    while(bet > 0) {
        var rollDice = Math.floor(Math.random()*6) + Math.floor(Math.random()*6) +2;
        currentStep = currentStep +1;
        if (rollDice == 7) {
            bet += 4;
        } else{
            bet -= 1;
        }
        if (bet>maxBet){
            maxBet = bet;
            maxBetStep = currentStep;
        }
        
        

        console.log([currentStep,rollDice, bet]); 
    }
    console.log("Initial bet" + " " + initialBet);
    console.log("Ending Roll" + " " + currentStep);
    console.log("Highest Amount Won" + " " + maxBet);
    console.log("Roll Count at Highest Amount Won" + " " +maxBetStep);
    

showResults(initialBet,currentStep,maxBet,maxBetStep); /*function call.*/

}

function showResults(firstBet,lastRoll,maxEarnings,highestEarningsStep) {   
 document.getElementById("results").style.display = "inline";
 document.getElementById("playbutton").innerHTML = "Play Again";
 document.getElementById("TableBet").innerHTML = "$" + firstBet + ".00";
 document.getElementById("TableFinalRoll").innerHTML = lastRoll;
 document.getElementById("TableHighestAmount").innerHTML = "$" + maxEarnings + ".00";
 document.getElementById("TableStepAtHighestAmount").innerHTML = highestEarningsStep;
 reset()
} 

function reset(){
    document.getElementById("betInput").value = " ";
}




